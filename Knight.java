import java.io.*;

/**
 * Knight
 */
public class Knight {
    private int baseHP;
    private int wp;

    Knight(int baseHP, int wp) {
        if ((baseHP < 1000) && (baseHP >= 99)) {
            this.baseHP = baseHP;
        } else {
            System.out.println("Strenght of the knight must from 99 to 999");
        }

        if ((wp < 4) && (wp >= 0)) {
            this.wp = wp;
        } else {
            System.out.println("Weapon of the knight is one of the values 0,1,2,3");
        }
    }

    public int getBaseHP() {
        return this.baseHP;
    }

    public int getwp() {
        return this.wp;
    }

    public int getRealHP() {
        int realHP = 0;
        if (this.getwp() == 1) {
            realHP = this.baseHP;
        } else if (this.getwp() == 0) {
            realHP = this.baseHP / 10;
        }
        return realHP;
    }
}