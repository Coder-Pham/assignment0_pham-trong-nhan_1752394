import java.io.*;

/**
 * Warrior
 */
public class Warrior {
    private int baseHP;
    private int wp;

    Warrior(int baseHP, int wp) {
        if ((baseHP <= 888) && (baseHP >= 1)) {
            this.baseHP = baseHP;
        } else {
            System.out.println("Strenght of the knight must from 1 to 888");
        }

        if ((wp < 4) && (wp >= 0)) {
            this.wp = wp;
        } else {
            System.out.println("Weapon of the knight is one of the values 0,1,2,3");
        }
    }

    public int getBaseHP() {
        return this.baseHP;
    }

    public int getwp() {
        return this.wp;
    }

    public int getRealHP() {
        int realHP = 0;
        if (this.getwp() == 1) {
            realHP = this.baseHP;
        } else if (this.getwp() == 0) {
            realHP = this.baseHP / 10;
        }
        return realHP;
    }
}